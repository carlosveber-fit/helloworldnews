package com.flex.hwnews

import android.os.Bundle
import android.util.Log
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.flex.hwnews.databinding.NewsReadBinding
import com.flex.hwnews.datac.News

class MainActivity : AppCompatActivity() {

    private var currentNews : News? = null
    private lateinit var newsReadBinding : NewsReadBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        this.newsReadBinding = NewsReadBinding.inflate(layoutInflater)

        this.currentNews = savedInstanceState?.getParcelable(STATE_KEY)

        loadView()
    }


    private fun loadView() {
        Log.i("MY_DEBUG", "View was loaded")
        setContentView(this.newsReadBinding.root)

        this.currentNews = this.currentNews ?: this.getOneNewsItem()

        this.loadNews(this.currentNews!!)
    }


    private fun getOneNewsItem(): News {
        Log.i("MY_DEBUG", "I was invoked")
        return News("Carlos", "Text", "Text", "Text", "Text", "Text", "Text", "Text")
    }


    private fun loadNews(item: News) {
        this.newsReadBinding.newsTitle.text = item.title
        this.newsReadBinding.newsSummary.text = item.summary
        this.newsReadBinding.newsContent.text = item.content
        this.newsReadBinding.newsAuthor.text = getString(R.string.news_author_and_source, item.author, item.source)
        this.newsReadBinding.lastUpdate.text = getString(R.string.news_updated_at, item.lastUpdate)

        loadImage()
    }

    private fun loadImage() {
        val imageView = findViewById<ImageView>(R.id.imageView)

        val url = "https://www.papiton.de/Bilder/Detail/steiff-teddybaer-1908-mohair-sandfarben-replikat-50-cm-403170.jpg"

        Glide
            .with(this)
            .asBitmap()
            .load(url)
            .placeholder(R.drawable.noimage)
            .into(imageView)
    }


    override fun onSaveInstanceState(outState: Bundle) {
        outState.putParcelable(STATE_KEY, this.currentNews)
        Log.i("MY_DEBUG", "saved")

        super.onSaveInstanceState(outState)
    }


    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        this.currentNews = savedInstanceState.getParcelable(STATE_KEY)
        Log.i("MY_DEBUG", "restored")

        super.onRestoreInstanceState(savedInstanceState)
    }


    companion object {
        const val STATE_KEY = "FRAGMENT_STATE"
    }
}