package com.flex.hwnews.contacts

class ContactRepository {

    private val contactsDatabase = mutableListOf<Contact>()

    fun upsert(model : Contact) : Int {
        if (model.id == null) model.id = getNextId()

        model.id?.let {
            this.removeById(model.id!!)
            this.contactsDatabase.add(model)
        }

        return model.id!!
    }

    private fun getNextId() = contactsDatabase.map { o -> o.id ?: 0 }.maxOrNull()?.plus(1)

    fun getById(id : Int) = contactsDatabase.find { o -> o.id == id }

    fun removeById(id : Int) {
        for(i in contactsDatabase.count()-1 downTo 0) {
            if (contactsDatabase[i].id == id) contactsDatabase.removeAt(i)
        }
        //contactsDatabase.removeIf { o -> o.id == id }
    }

    fun search(term : String) = contactsDatabase.find { o ->
        o.firstName.contains(term) ||
        o.lastName.contains(term) ||
        o.nickName?.contains(term) ?: false
    }
}