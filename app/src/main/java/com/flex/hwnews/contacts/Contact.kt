package com.flex.hwnews.contacts

data class Contact(
        val firstName: String,
        val lastName : String,
        val nickName : String?,
        val phoneNumber: String,
        var id : Int?)
