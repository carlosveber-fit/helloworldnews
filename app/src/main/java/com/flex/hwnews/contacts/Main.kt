package com.flex.hwnews.contacts

fun main() {

    val contactsDb = ContactRepository()

    populate(contactsDb)

    contactsDb.getById(3)?.print()

    contactsDb.getById(111)?.print()

    println( "Get Contact 3: ${contactsDb.getById(3)?.firstName}" )

    println( "Get Contact 1: ${contactsDb.getById(1)?.firstName}" )

    update(contactsDb)

    println("Update contact 1: ${contactsDb.getById(1)?.nickName}")

    remove(contactsDb)

    println("Remove contact 1: ${contactsDb.getById(1)?.nickName}")

    println("Search Maria: ${contactsDb.search("Maria")}")

}

fun Contact.print() {
    print(this)
}

private fun remove(contactsDb: ContactRepository) {
    contactsDb.removeById(1)
}

private fun update(contactsDb: ContactRepository) {
    contactsDb.upsert(Contact(
            "Carlos",
            "Veber",
            "Vebito",
            "(15) 98123-12121",
            1
    ))
}

private fun populate(contactsDb: ContactRepository) {
    contactsDb.upsert(Contact(
            "Carlos",
            "Veber",
            null,
            "(15) 98123-12121",
            1))

    contactsDb.upsert(Contact(
            "Jose",
            "Carlos",
            null,
            "(15) 98123-12121",
            2))

    contactsDb.upsert(Contact(
            "Maria",
            "Santos",
            null,
            "(15) 98123-12121",
            3))
}

